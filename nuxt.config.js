const webpack = require('webpack')
const pkg = require('./package')
var axios = require('axios'); 
module.exports = {
  mode: 'spa',

  /*
   ** Headers of the page
   */
  head: {
    title: '思儒国际留学 Siru International',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script:[
      {src:'https://js.createsend1.com/javascript/copypastesubscribeformlogic.js'}
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Global CSS
   */
  css: [
    '@/assets/css/main.scss'
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [

    {src: '~/plugins/aos', ssr:false},
    {src: '~/plugins/slick', ssr:false},
    {src: '~/plugins/scrollspy', ssr:false},
    {src: '~/plugins/axios', ssr:false}
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/pwa'
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  generate: {
    routes: function () {
      return axios.get('https://blog.sirucanada.com/wp-json/wp/v2/posts?_embed')
      .then((res) => {
        return res.data.map((user) => {
          return '/posts/' + user.id
        })
      })
    }
  },
  build: {
    /*
     ** You can extend webpack config here
     */
    vendor: ["jquery", "bootstrap", "vue2-google-maps"],
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    extend(config, ctx) {
      // Run ESLint on save
      /*if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }*/
    }
  }
}
