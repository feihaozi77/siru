import Vuex from 'vuex'
console.log(process.env)
const store = () => new Vuex.Store({
  
  state: {
    posts:[],
    wordpressAPI: 'https://blog.sirucanada.com/2/wp-json',
      activeVector: 6
  },

  mutations: {
    setPost (state, data) {
      state.posts = data
    },
    setVector (state, num){
      state.activeVector = num
    }
  }
})
export default store
